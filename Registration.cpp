#include <cstddef>
#include <filesystem>
#include <format>
#include <stdexcept>
#include <string>

#include <SoapySDR/Registry.hpp>
#include <SoapySDR/Types.hpp>
#include <SoapySDR/Version.hpp>

#include "SoapyFileIO.hpp"

static SoapySDR::KwargsList findSoapyFileIO(const SoapySDR::Kwargs &args)
{
    if (args.contains(filePathArgString))
    {
        const std::filesystem::path filePath(args.at(filePathArgString));
        if (std::filesystem::exists(filePath))
        {
            return {{{filePathArgString, filePath}}};
        }
    }
    return {};
}

static SoapySDR::Device *makeSoapyFileIO(const SoapySDR::Kwargs &args)
{
    std::filesystem::path filePath;
    auto streamFormat(defaultStreamFormat);
    auto numChannels(defaultNumChannels);

    if (args.contains(filePathArgString))
    {
        filePath = args.at(filePathArgString);
    }
    else
    {
        throw std::invalid_argument(std::format("missing {}", filePathArgString));
    }

    if (args.contains(streamFormatArgString))
    {
        streamFormat = args.at(streamFormatArgString);
    }

    if (args.contains(numChannelsArgString))
    {
        numChannels = std::stoi(args.at(numChannelsArgString));
    }

    return new SoapyFileIO(filePath, streamFormat, numChannels);
}

static SoapySDR::Registry soapyFileIORegistry(driverName, &findSoapyFileIO, &makeSoapyFileIO, SOAPY_SDR_ABI_VERSION);
