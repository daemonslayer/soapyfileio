#include <cstddef>
#include <filesystem>
#include <string>

#include <SoapySDR/Types.hpp>

#include "SoapyFileIO.hpp"

SoapyFileIO::SoapyFileIO(
    const std::filesystem::path &filePath_,
    const std::string &streamFormat_,
    const std::size_t numChannels_)
    : filePath(filePath_)
    , streamFormat(streamFormat_)
    , numChannels(numChannels_)
{
}

SoapyFileIO::~SoapyFileIO()
{
}

std::string SoapyFileIO::getDriverKey() const
{
    return driverKey;
}

std::string SoapyFileIO::getHardwareKey() const
{
    return hardwareKey;
}

SoapySDR::Kwargs SoapyFileIO::getHardwareInfo() const
{
    return {
        {filePathArgString, filePath},
        {streamFormatArgString, streamFormat},
        {numChannelsArgString, std::to_string(numChannels)},
    };
}

std::size_t SoapyFileIO::getNumChannels(const int direction) const
{
    (void)direction;

    return numChannels;
}

bool SoapyFileIO::getFullDuplex(const int direction, const std::size_t channel) const
{
    (void)direction;
    (void)channel;

    return false;
}
