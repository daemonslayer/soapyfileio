#include <cstddef>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <ios>
#include <string>
#include <vector>

#include <SoapySDR/Formats.hpp>
#include <SoapySDR/Types.hpp>

#include "SoapyFileIO.hpp"

static double formatToFullScale(const std::string &format)
{
    return std::map<std::string, double>{
        {SOAPY_SDR_CF64, 1.},
        {SOAPY_SDR_CF32, 1.},
        {SOAPY_SDR_CS32, 2147483648.},
        {SOAPY_SDR_CU32, 4294967296.},
        {SOAPY_SDR_CS16, 32768.},
        {SOAPY_SDR_CU16, 65535.},
        {SOAPY_SDR_CS12, 2048.},
        {SOAPY_SDR_CU12, 4096.},
        {SOAPY_SDR_CS8, 128.},
        {SOAPY_SDR_CU8, 256.},
        {SOAPY_SDR_CS4, 8.},
        {SOAPY_SDR_CU4, 16.},
        {SOAPY_SDR_F64, 1.},
        {SOAPY_SDR_F32, 1.},
        {SOAPY_SDR_S32, 2147483648.},
        {SOAPY_SDR_U32, 4294967296.},
        {SOAPY_SDR_S16, 32768.},
        {SOAPY_SDR_U16, 65535.},
        {SOAPY_SDR_S8, 128.},
        {SOAPY_SDR_U8, 256.},
    }
        .at(format);
}

static std::size_t readStreamByElems(SoapySDR::Stream *stream, char *inBuff, std::size_t numElems, std::size_t elemSize)
{
    stream->read(inBuff, numElems * elemSize);
    const auto numBytesRead(stream->gcount());
    const auto numElemsRead(numBytesRead / elemSize);
    const auto numBytesRemain(numBytesRead % elemSize);
    stream->seekg(-numBytesRemain, std::ios::cur);
    return numElemsRead;
}

SoapySDR::Stream::Stream(const std::filesystem::path &filePath, std::ios::openmode mode)
    : std::fstream(filePath, mode)
{
}

SoapySDR::Stream::~Stream()
{
}

std::vector<std::string> SoapyFileIO::getStreamFormats(const int direction, const std::size_t channel) const
{
    (void)direction;
    (void)channel;

    return {streamFormat};
}

std::string SoapyFileIO::getNativeStreamFormat(const int direction, const std::size_t channel, double &fullScale) const
{
    (void)direction;
    (void)channel;

    fullScale = formatToFullScale(streamFormat);
    return streamFormat;
}

SoapySDR::Stream *SoapyFileIO::setupStream(
    const int direction,
    const std::string &format,
    const std::vector<std::size_t> &channels,
    const SoapySDR::Kwargs &args)
{
    (void)direction;
    (void)format;
    (void)channels;
    (void)args;

    const auto mode(direction == SOAPY_SDR_RX ? std::ios::in : std::ios::out);
    return new SoapySDR::Stream(filePath, mode);
}

void SoapyFileIO::closeStream(SoapySDR::Stream *stream)
{
    delete stream;
}

int SoapyFileIO::activateStream(
    SoapySDR::Stream *stream,
    const int flags,
    const long long timeNs,
    const std::size_t numElems)
{
    (void)stream;
    (void)flags;
    (void)timeNs;
    (void)numElems;

    return 0;
}

int SoapyFileIO::deactivateStream(SoapySDR::Stream *stream, const int flags, const long long timeNs)
{
    (void)stream;
    (void)flags;
    (void)timeNs;

    return 0;
}

int SoapyFileIO::readStream(
    SoapySDR::Stream *stream,
    void *const *buffs,
    const std::size_t numElems,
    int &flags,
    long long &timeNs,
    const long timeoutUs)
{

    (void)flags;
    (void)timeNs;
    (void)timeoutUs;

    const auto sampleSize(SoapySDR::formatToSize(streamFormat));
    const auto inElemSize(numChannels * sampleSize);

    std::size_t numElemsRead;
    char inBuff[numElems * inElemSize];
    try
    {
        numElemsRead = readStreamByElems(stream, inBuff, numElems, inElemSize);
    }
    catch (const std::ios::failure &)
    {
        return SOAPY_SDR_STREAM_ERROR;
    }

    const auto outBuffs(reinterpret_cast<char *const *>(buffs));
    for (std::size_t elem = 0; elem < numElemsRead; elem++)
    {
        for (std::size_t chan = 0; chan < numChannels; chan++)
        {
            const auto inSample(&inBuff[(elem * inElemSize) + chan * sampleSize]);
            const auto outSample(&outBuffs[chan][elem * sampleSize]);
            (void)memcpy(outSample, inSample, sampleSize);
        }
    }
    return numElemsRead;
}

int SoapyFileIO::readStreamStatus(
    SoapySDR::Stream *stream,
    std::size_t &chanMask,
    int &flags,
    long long &timeNs,
    const long timeoutUs)
{
    (void)chanMask;
    (void)flags;
    (void)timeNs;
    (void)timeoutUs;

    return stream->good() ? 0 : SOAPY_SDR_STREAM_ERROR;
}

void *SoapyFileIO::getNativeDeviceHandle() const
{
    return nullptr;
}
