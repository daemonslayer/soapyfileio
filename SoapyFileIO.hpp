#pragma once

#include <cstddef>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>

#include <SoapySDR/Device.hpp>
#include <SoapySDR/Formats.hpp>
#include <SoapySDR/Types.hpp>

static constexpr std::string driverName("fileio");
static constexpr std::string driverKey("FileIO");
static constexpr std::string hardwareKey("FileIO");

static constexpr std::string filePathArgString("file");
static constexpr std::string streamFormatArgString("format");
static constexpr std::string numChannelsArgString("channels");

// rx_tools (incorrectly) assumes this is the default for all drivers
// https://github.com/rxseger/rx_tools/blob/0af9527a19582cfd805c943ecb60cc3f0f6c1be8/src/rtl_sdr.c#L138
static constexpr std::string defaultStreamFormat(SOAPY_SDR_CS16);
static constexpr std::size_t defaultNumChannels(1UZ);

class SoapySDR::Stream : public std::fstream
{
  public:
    Stream(const std::filesystem::path &path, std::ios::openmode mode = std::ios::in);

    ~Stream();
};

class SoapyFileIO : public SoapySDR::Device
{
  private:
    std::filesystem::path filePath;
    std::string streamFormat;
    std::size_t numChannels;

  public:
    SoapyFileIO(const std::filesystem::path &path, const std::string &streamFormat, const std::size_t numChannels);

    ~SoapyFileIO();

    std::string getDriverKey() const;

    std::string getHardwareKey() const;

    SoapySDR::Kwargs getHardwareInfo() const;

    std::size_t getNumChannels(const int direction) const;

    bool getFullDuplex(const int direction, const std::size_t channel) const;

    std::vector<std::string> getStreamFormats(const int direction, const std::size_t channel) const;

    std::string getNativeStreamFormat(const int direction, const std::size_t channel, double &fullScale) const;

    SoapySDR::Stream *setupStream(
        const int direction,
        const std::string &format,
        const std::vector<std::size_t> &channels = std::vector<std::size_t>(),
        const SoapySDR::Kwargs &args = SoapySDR::Kwargs());

    void closeStream(SoapySDR::Stream *stream);

    int activateStream(
        SoapySDR::Stream *stream,
        const int flags = 0,
        const long long timeNs = 0LL,
        const std::size_t numElems = 0UZ);

    int deactivateStream(SoapySDR::Stream *stream, const int flags = 0, const long long timeNs = 0LL);

    int readStream(
        SoapySDR::Stream *stream,
        void *const *buffs,
        const std::size_t numElems,
        int &flags,
        long long &timeNs,
        const long timeoutUs = 100000L);

    int readStreamStatus(
        SoapySDR::Stream *stream,
        std::size_t &chanMask,
        int &flags,
        long long &timeNs,
        const long timeoutUs = 100000L);

    void *getNativeDeviceHandle() const;
};
