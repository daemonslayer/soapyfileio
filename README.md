# File I/O module for SoapySDR

## Building and Installation

```sh
cmake -B ./build/ -DCMAKE_CXX_FLAGS='-std=c++2b'
cmake --build ./build/
cmake --install ./build/
```
